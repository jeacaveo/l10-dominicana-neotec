# -*- coding: utf-8 -*-
{
    'name': "Estados Bancarios Dominicanos",

    'summary': """
        Este módulo facilita la importación de estados bancarios de bancos dominicanos""",

    'author': "Marcos Organizador de Negocios SRL, "
              "iterativo SRL, "
              "Neotec, "
              "Odoo Dominicana (ODOM)",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Accounting',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'account_bank_statement_import'],

    # always loaded
    'data': [
        'views/account_bank_statement_import_view.xml',
    ],
}