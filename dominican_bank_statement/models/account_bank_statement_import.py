# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class AccountBankStatementImport(models.TransientModel):
    _inherit = 'account.bank.statement.import'

    bank = fields.Selection([
        ('popular', 'Banco Popular Dominicano'),
        ('bhd', 'Banco BHD'),
        ('progreso', 'Banco Progreso'),
        ('reserva', 'Banco de Reserva'),
    ])

    def _validate_file_extension(self, filename, bank):
        """Depending on bank, validate uploaded file extension"""
        if bank == 'popular':
            return True if filename.endswith('.txt') else False
        elif bank == 'bhd':
            return True if filename.endswith('.csv') else False
        elif bank == 'progreso':
            return True if filename.endswith('.csv') else False
        elif bank == 'reserva':
            return True if filename.endswith('.csv') else False

    def _get_partner_id(self, account):
        """Returns given account partner_id, if exists"""
        bank_id = self.env['res.partner.bank'].search([
            ('acc_number', '=', account.lstrip('0'))])

        return bank_id.partner_id.id if bank_id.partner_id else False

    def _get_journal_id(self, account):
        """Return journal if file bank account match journal bank account"""
        journal_id = self.env['account.journal'].browse(self.env.context.get(
            'journal_id'))
        if account == journal_id.bank_account_id.acc_number:
            return journal_id
        else:
            raise ValidationError(_('Error. Statement bank account doesn\'t '
                                    'match this journal account.'))

    # def _validate_data_index(self, file, rec):
    #     """Depending on bank, validate file data index"""
    #     index = False
    #     if rec.bank == 'bpd':
    #         index = 8
    #     elif rec.bank=='bhd':
    #         index = 10
    #     elif rec.bank=='bpg':
    #         index = 4
    #
    #     if len(file.readline().split(",")) != index:
    #         raise ValidationError(_('Error. Invalid data format.'))

    @api.multi
    def import_dominican_statement(self):
        for rec in self:
            if rec.data_file:
                if self._validate_file_extension(rec.filename, rec.bank):

                    if rec.bank == 'popular':
                        self._import_popular_statement(rec)
                    elif rec.bank == 'bhd':
                        self._import_bhd_statement(rec)
                    elif rec.bank == 'progreso':
                        self._import_progreso_statement(rec)
                    elif rec.bank == 'reserva':
                        self._import_reserva_statement(rec)

                else:
                    raise ValidationError(_('Error. Wrong file extension.'))
