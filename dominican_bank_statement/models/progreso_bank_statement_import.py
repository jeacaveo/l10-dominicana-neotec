import re
import base64
from datetime import datetime as dt

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class AccountBankStatementImport(models.TransientModel):
    _inherit = 'account.bank.statement.import'

    def _validate_data_index(self, file, rec):
        if rec.bank == 'progreso':
            line = file.readlines()
            if len(line[-1].split(";")) != 4:
                raise ValidationError(_('Error. Invalid data format.'))

        return True

    def _import_progreso_statement(self, rec):
        """
        Process data from txt and generate bank statement for Banco BHD
        :param rec: account.bank.statement.import record
        """
        # Convert binary data to latin1
        values = self._prepare_dict(rec.data_file)
        data = base64.b64decode(rec.data_file)
        with open('/tmp/statement.txt', 'w') as w_file:
             w_file.write(data.decode('latin1'))
        with open('/tmp/statement.txt', 'r') as r_file:
            res = self._validate_data_index(r_file, rec)
            # Get journal
            account = values['acc_number']
            journal_id = self._get_journal_id(account)

            time_now = dt.now().strftime("%Y-%m-%d")
            statement_id = self.env['account.bank.statement'].create(
                {
                    'name': "{} {}".format(journal_id.name, time_now),
                    'date': time_now,
                    'journal_id': journal_id.id,
                })

            for item in values['data']:
                # This is to prevent error data from extra empty column
                if len(item.keys()) != 4:
                    continue

                file_date = item.get('Fecha').rstrip()
                date = dt.strptime(file_date, "%d/%m/%Y").strftime("%Y-%m-%d")
                document = item.get('Documento')
                reference = document if document != '0' else False

                vals = {
                    'date': date,
                    'name': item.get('Descripcion'),
                    'ref': reference,
                    'amount': float(item.get('Monto').replace(',', ''))
                }
                statement_id.line_ids = [[0, 0, vals]]

    def _prepare_dict(self, data_file):
        decoded_data = base64.b64decode(data_file)
        data = str(decoded_data, 'utf-8')
        header = False
        values_dict = {'data': []}
        for record in (z.split(';') for z in data.split('\n')):
            if record[0] and not record[1]:
                acc_number = record[0].split(' ')
                values_dict['acc_number'] = acc_number[2]
                continue
            elif not header:
                header = record
                header[-1] = record[-1].replace("\r","")
                continue
            else:
                record[-1] = record[-1].replace("\r", "")
                values_dict['data'].append(dict(zip(header, record)))
        return values_dict

    def _validate_file_extension(self, filename, bank):
        res = super(AccountBankStatementImport, self)._validate_file_extension(
            filename, bank)

        if bank == 'progreso':
            if filename.endswith('.csv'):
                return True
            else:
                return res
        else:
            return res

    @api.multi
    def import_dominican_statement(self):
        for rec in self:
            if rec.data_file:
                if rec.bank == 'progreso':
                    if self._validate_file_extension(rec.filename, rec.bank):
                        self._import_progreso_statement(rec)
                    else:
                        raise ValidationError(_('Error. La extension del '
                                                'archivo debe ser .csv.'))

        return super(AccountBankStatementImport, self).import_dominican_statement()

