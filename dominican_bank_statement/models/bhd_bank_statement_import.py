import re
import base64
from datetime import datetime as dt

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError


class AccountBankStatementImport(models.TransientModel):
    _inherit = 'account.bank.statement.import'

    bank = fields.Selection(selection_add=[('bhd', 'Banco BHD')])

    def _validate_data_index(self, file, rec):
        if rec.bank == 'bhd':
            if len(file.readline().split(",")) != 10:
                raise ValidationError(_('Error. Invalid data format.'))

        return super(AccountBankStatementImport, self)._validate_data_index(file, rec)

    def _import_bhd_statement(self, rec):
        """
        Process data from txt and generate bank statement for Banco BHD
        :param rec: account.bank.statement.import record
        """
        # Convert binary data to latin1
        data = base64.b64decode(rec.data_file)
        with open('/tmp/statement.txt', 'w') as w_file:
             w_file.write(data.decode('latin1'))

        with open('/tmp/statement.txt', 'r') as r_file:
            res = self._validate_data_index(r_file, rec)
            #r_file.seek(0)
            first_line = r_file.readlines()[0]
            print("RES: %s" % first_line)

            # Get journal
            account = first_line.split(",")[0].replace('"', '')
            journal_id = self._get_journal_id(account)

            time_now = dt.now().strftime("%Y-%m-%d")
            statement_id = self.env['account.bank.statement'].create(
                {
                    'name': "{} {}".format(journal_id.name, time_now),
                    'date': time_now,
                    'journal_id': journal_id.id,
                    'balance_start': first_line.split(',"')[1].rstrip().replace(',', '').replace('"', ''),
                    'balance_end_real': first_line.split(',"')[4].rstrip().replace(',', '').replace('"', '')
                })

        # Write statement lines
        with open('/tmp/statement.txt', 'r') as data:
            header = False
            for line in data:
                if not header:
                    header = True
                    continue
                fline = re.split(',(?=\S)', line.replace('"', ''))
                vals = {
                    'date': dt.strptime(fline[0].rstrip(), "%d/%m/%Y").strftime("%Y-%m-%d"),
                    'name': fline[4],
                    'amount': fline[6] if fline[6] != '0' else str(float(fline[5]) * - 1)
                }
                statement_id.line_ids = [[0, 0, vals]]

    def _validate_file_extension(self, filename, bank):
        res = super(AccountBankStatementImport, self)._validate_file_extension(filename, bank)

        if bank == 'bhd':
            if filename.endswith('.csv'):
                return True
            else:
                return res
        else:
            return res

    @api.multi
    def import_dominican_statement(self):
        for rec in self:
            if rec.data_file:
                if rec.bank == 'bhd':
                    if self._validate_file_extension(rec.filename, rec.bank):
                        self._import_bhd_statement(rec)
                    else:
                        raise ValidationError(_('Error. Wrong file extension.'))

        return super(AccountBankStatementImport, self).import_dominican_statement()
