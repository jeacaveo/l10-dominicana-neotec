# -*- coding: utf-8 -*-

import base64
from datetime import datetime as dt

from odoo import models


class AccountBankStatementImport(models.TransientModel):
    _inherit = 'account.bank.statement.import'

    def _import_popular_statement(self, rec):
        """
        Process data from txt and generate bank statement for Banco Popular
        :param rec: account.bank.statement.import record
        """

        # Convert binary data to latin1
        data = base64.b64decode(rec.data_file)
        with open('/tmp/statement.txt', 'w+') as w_file:
            w_file.write(data.decode('latin1'))

        with open('/tmp/statement.txt', 'r+') as r_file:

            self._validate_data_index(r_file, rec)

            # Get journal
            account = r_file.readline().split(",")[0].lstrip('0')
            journal_id = self._get_journal_id(account)

        time_now = dt.now().strftime("%Y-%m-%d")
        statement_id = self.env['account.bank.statement'].create(
            {
                'name': "{} {}".format(journal_id.name, time_now),
                'date': time_now,
                'journal_id': journal_id.id
            })

        # Write statement lines
        with open('/tmp/statement.txt', 'r') as data:
            for line in data:
                amount = line.split(",")[3].lstrip('0')
                vals = {
                    'date': dt.strptime(line.split(",")[1], "%d/%m/%Y").strftime("%Y-%m-%d"),
                    'partner_id': self._get_partner_id(line.split(",")[2]),
                    'name': line.split(",")[5],
                    'amount': amount if line.split(",")[4] == 'CR' else str(float(amount) * -1)
                }
                statement_id.line_ids = [[0, 0, vals]]
