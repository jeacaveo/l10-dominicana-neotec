# © 2018-2019 Yasmany Castillo <yasmany003@gmail.com>

# This file is part of NCF Manager.

# NCF Manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# NCF Manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with NCF Manager.  If not, see <https://www.gnu.org/licenses/>.

import logging

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

NCF_SALE_TYPE = {
    'B01': 'fiscal',
    'B02': 'final',
    'B04': 'credit_note',
}

class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.model
    def create(self, vals):
        if vals.get('origin'):
            shop = vals.get('origin').split('/')[0]
            pos_config = self.env['pos.config'].search([('name', '=', shop)])
            if pos_config:
                number = vals.get('reference')
                if number[:3] in NCF_SALE_TYPE:
                    vals.update({
                        'ncf_number': number,
                        'number': number,
                        'move_name': number
                    })

        return super(AccountInvoice, self).create(vals)
