# -*- coding: utf-8 -*-

from . import res_partner
from . import dgii_report
from . import account_invoice
from . import account_account
