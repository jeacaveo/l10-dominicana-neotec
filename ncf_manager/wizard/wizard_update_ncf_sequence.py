from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class WizardUpdateNcfSequence(models.TransientModel):
    _name = 'wizard.update.ncf.sequence'
    _description = "Model to register DGII auth"

    def _get_current_ncf(self):
        active_id = self._context.get('active_id', False)
        if active_id:
            return self.env['ncf.manager'].browse(active_id)

    reason = fields.Char(string='Razón', required=True, )
    ncf_id = fields.Many2one(
        comodel_name='ncf.manager',
        readonly=True,
        default=_get_current_ncf,
        string="Comprobante",
    )
    ncf_type = fields.Selection(
        string='NCF type',
        related="ncf_id.type",
    )
    sale_type = fields.Selection(
        string='Sale type',
        related="ncf_id.sale_type",
    )
    purchase_type = fields.Selection(
        string='Purchase type',
        related="ncf_id.purchase_type",
    )
    number_next = fields.Char(
        string='Actual sequence',
        related="ncf_id.number_next",
    )
    qty_requested = fields.Integer(string='Cantidad solicitada')
    qty_approved = fields.Integer(string='Cantidad aprobada')
    request_no = fields.Char(string='No. Solicitud')
    request_date = fields.Date(string='Fecha de solicitud')
    auth_number = fields.Char(string='No. Autorización', )
    from_number = fields.Integer(string='Número Desde', )
    to_number = fields.Integer(string='Número Hasta', )
    due_date = fields.Date(string='Fecha Vencimiento', )
    next_authorization = fields.Boolean(string='Proxima autorización', )

    @api.onchange('request_no', 'auth_number')
    def _onchange_number(self):
        if self.request_no and not self.request_no.isdigit():
            raise ValidationError("Este campo solo admite números")
        if self.auth_number and not self.auth_number.isdigit():
            raise ValidationError("Este campo solo admite números")

    @api.multi
    def update_ncf_info(self):
        ncf_log_obj = self.env['ncf.change.log']
        if self.from_number > self.to_number:
            raise ValidationError(
                _('Número Desde, no puede ser mayor que Número Hasta.'))

        if not self.next_authorization:
            for rec in self.ncf_id.change_log_ids:
                rec.update({'state': 'expired'})

        values = {
            'reason': self.reason,
            'ncf_id': self.ncf_id.id,
            'request_no': self.request_no,
            'request_date': self.request_date,
            'auth_number': self.auth_number,
            'from_number': self.from_number,
            'to_number': self.to_number,
            'due_date': self.due_date,
            'update_by': self.env.user.id,
            'company_id': self.env.user.company_id.id,
            'next_authorization': self.next_authorization,
        }
        ncf_log_obj.create(values)
