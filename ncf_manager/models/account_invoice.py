# © 2015-2018 Eneldo Serrata <eneldo@marcos.do>
# © 2017-2018 Gustavo Valverde <gustavo@iterativo.do>
# © 2018-2019 Yasmany Castillo <yasmany003@gmail.com>
# © 2018 José López <jlopez@indexa.do>
# © 2018 Kevin Jiménez <kevinjimenezlorenzo@gmail.com>
# © 2018 Francisco Peñaló <frankpenalo24@gmail.com>
# © 2018 Andrés Rodríguez <andres@iterativo.do>

# This file is part of NCF Manager.

# NCF Manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# NCF Manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with NCF Manager.  If not, see <https://www.gnu.org/licenses/>.

import logging

from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError

_logger = logging.getLogger(__name__)

try:
    from stdnum.do import ncf as ncf_validation
    from stdnum.do import rnc as rnc_validation
except (ImportError, IOError) as err:
    _logger.debug(err)


class AccountInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    @api.depends('state')
    def _compute_ncf_expiration_date(self):
        for inv in self:
            if inv.type == 'out_invoice' and inv.state != 'draft':
                if inv.sale_fiscal_type and inv.ncf_id and \
                                inv.ncf_id.type == 'sale':
                    msg = self._invoice_warnings('ncf_expiration')
                    try:
                        change_log = inv.ncf_id.change_log_ids.search([
                            ('state', '=', 'active'),
                            ('ncf_id', '=', inv.ncf_id.id),
                            ('next_authorization', '=', False)
                        ], limit=1)
                        if change_log:
                            inv.ncf_expiration_date = change_log.due_date
                        else:
                            raise ValidationError(_(msg % inv.ncf_id.name))
                    except IndexError:
                        msg = self._invoice_warnings('ncf_expiration')
                        raise ValidationError(_(msg % inv.ncf_id.name))

    @api.depends('journal_id', 'sale_fiscal_type', 'ncf_id')
    def _compute_sequence_almost_depleted(self):
        for invoice in self:
            inv_type = "sale" if invoice.type == "out_invoice" else "purchase"
            if invoice.ncf_id:
                ncf = invoice.ncf_id
            else:
                domain = [
                    ('company_id', '=', self.company_id.id),
                    ('journal_id', 'in', self.journal_id.ids),
                    ('type', '=', inv_type),
                    ('sale_type', '=', self.sale_fiscal_type)
                ]
                ncf = self.env['ncf.manager'].search(domain, limit=1)

            if ncf:
                sequence = ncf.sequence_id
                active_auth = ncf.change_log_ids.search([
                    ('state', '=', 'active'),
                    ('ncf_id', '=', ncf.id),
                    ('next_authorization', '=', False)
                ], limit=1)
                alert_number = active_auth.set_alert_number()

                if sequence.number_next_actual >= alert_number:
                    invoice.sequence_almost_depleted = True
                else:
                    invoice.sequence_almost_depleted = False

    sequence_almost_depleted = fields.Boolean(
        compute="_compute_sequence_almost_depleted")
    ncf_number = fields.Char(
        string='Fiscal Number',
        copy=False,
        size=32,
    )
    ncf_control = fields.Boolean(related="journal_id.ncf_control")
    purchase_type = fields.Selection(related="journal_id.purchase_type")
    sale_fiscal_type = fields.Selection(
        string='NCF para',
        selection=[
            ("final", "Consumo"),
            ("fiscal", u"Crédito Fiscal"),
            ("gov", "Gubernamentales"),
            ("special", u"Regímenes Especiales"),
            ("unico", u"Único Ingreso"),
            ("export", u"Exportaciones"),
        ],
        default=lambda self: self._context.get('sale_fiscal_type', 'final'),
    )
    income_type = fields.Selection(
        string='Tipo de Ingreso',
        selection=[
            ('01', '01 - Ingresos por Operaciones (No Financieros)'),
            ('02', '02 - Ingresos Financieros'),
            ('03', '03 - Ingresos Extraordinarios'),
            ('04', '04 - Ingresos por Arrendamientos'),
            ('05', '05 - Ingresos por Venta de Activo Depreciable'),
            ('06', '06 - Otros Ingresos'),
        ],
        default=lambda self: self._context.get('income_type', '01'),
    )
    expense_type = fields.Selection(
        string="Tipo de Costos y Gastos",
        selection=[
            ('01', '01 - Gastos de Personal'),
            ('02', '02 - Gastos por Trabajo, Suministros y Servicios'),
            ('03', '03 - Arrendamientos'), ('04', '04 - Gastos de Activos Fijos'),
            ('05', u'05 - Gastos de Representación'),
            ('06', '06 - Otras Deducciones Admitidas'),
            ('07', '07 - Gastos Financieros'),
            ('08', '08 - Gastos Extraordinarios'),
            ('09', '09 - Compras y Gastos que forman parte del Costo de Venta'),
            ('10', '10 - Adquisiciones de Activos'),
            ('11', '11 - Gastos de Seguros'),
        ],
    )
    anulation_type = fields.Selection(
        string=u"Tipo de anulación",
        selection=[
            ("01", "01 - Deterioro de Factura Pre-impresa"),
            ("02", u"02 - Errores de Impresión (Factura Pre-impresa)"),
            ("03", u"03 - Impresión Defectuosa"),
            ("04", u"04 - Corrección de la Información"),
            ("05", "05 - Cambio de Productos"),
            ("06", u"06 - Devolución de Productos"),
            ("07", u"07 - Omisión de Productos"),
            ("08", "08 - Errores en Secuencia de NCF"),
            ("09", "09 - Por Cese de Operaciones"),
            ("10", u"10 - Pérdida o Hurto de Talonarios"),
        ],
        copy=False,
    )
    is_company_currency = fields.Boolean(compute='_is_company_currency')
    invoice_rate = fields.Monetary(
        string="Tasa",
        compute='_get_rate',
        currency_field='currency_id',
    )
    is_nd = fields.Boolean()
    origin_out = fields.Char("Afecta a")
    ncf_expiration_date = fields.Date(
        string='NCF Válido hasta',
        compute="_compute_ncf_expiration_date",
        readonly=True,
        store=True,
    )
    ncf_id = fields.Many2one(
        comodel_name='ncf.manager',
        string="NCF Structure",
        required=False,
        readonly=True,
        store=True,
    )

    @api.multi
    @api.depends("currency_id")
    def _is_company_currency(self):
        for inv in self:
            if inv.currency_id == inv.company_id.currency_id:
                inv.is_company_currency = True
            else:
                inv.is_company_currency = False

    @api.multi
    @api.depends('currency_id', "date_invoice")
    def _get_rate(self):
        for inv in self:
            if not inv.is_company_currency:
                try:
                    rate = inv.currency_id.with_context(
                        dict(self._context or {}, date=inv.date_invoice))
                    inv.invoice_rate = 1 / rate.rate
                    inv.rate_id = rate.res_currency_rate_id
                except (Exception) as err:
                    _logger.debug(err)

    @api.onchange('partner_id', 'company_id')
    def _onchange_partner_id(self):
        res = super(AccountInvoice, self)._onchange_partner_id()
        if not self.partner_id:
            return res
        if self.partner_id and self.type == 'out_invoice':
            if self.journal_id.ncf_control:
                self.special_check()
                fiscal_type = self.partner_id.sale_fiscal_type
                self._set_ncf_structure(fiscal_type)
                self.sale_fiscal_type = fiscal_type
                if fiscal_type == 'special':
                    carnet_expired = self._check_carnet_expiration_date()
                    if carnet_expired:
                        if res:
                            res.update({'warning': carnet_expired})
                        else:
                            res = {'warning': carnet_expired}
                    return res
            if not self.partner_id.customer:
                self.partner_id.customer = True
        elif not self.partner_id and self.type == 'in_invoice':
            self.ncf_id = False
        elif self.partner_id and self.type == 'in_invoice':
            self.expense_type = self.partner_id.expense_type
            if self.journal_id.purchase_type:
                self._set_ncf_structure(self.journal_id.purchase_type)
            if not self.partner_id.supplier:
                self.partner_id.supplier = True
        return res

    @api.onchange('journal_id', 'partner_id')
    def onchange_journal_id(self):
        res = super(AccountInvoice, self)._onchange_journal_id()
        if self.journal_id.type == 'purchase':
            if self.journal_id.purchase_type == "minor":
                self.partner_id = self.company_id.partner_id.id
                journal_id = self.env['account.journal'].search([
                    ('purchase_type', '=', 'minor'),
                    ('company_id', '=', self.company_id.id)
                ])
                if not journal_id:
                    raise ValidationError(
                        _(self._invoice_warnings('minor')))
                self.journal_id = journal_id.id
        if self.journal_id.type == 'sale' and not self.journal_id.ncf_control:
            self.ncf_id = False
        else:
            fiscal_type = self.sale_fiscal_type or self.journal_id.purchase_type
            self._set_ncf_structure(fiscal_type)
        return res

    @api.onchange('sale_fiscal_type', 'expense_type')
    def _onchange_fiscal_type(self):
        if self.partner_id:
            fiscal_type = self.sale_fiscal_type
            if self.type == 'out_invoice' and not self.journal_id.ncf_control:
                self.partner_id.write({
                    'ncf_id': False,
                    'sale_fiscal_type': fiscal_type
                })
            if self.type == 'out_invoice' and self.journal_id.ncf_control:
                self._set_ncf_structure(fiscal_type)
                self.partner_id.write(
                    {'sale_fiscal_type': fiscal_type})
                self.special_check()
            if self.type == 'in_invoice':
                if self.journal_id.purchase_type:
                    self._set_ncf_structure(self.journal_id.purchase_type)
                self.partner_id.write({'expense_type': self.expense_type})

    @api.onchange("ncf_number", "origin_out")
    def onchange_ncf(self):
        if self.journal_id.purchase_type in ('normal', 'informal', 'minor'):
            self.validate_fiscal_purchase()

        if self.origin_out and (self.type == 'out_refund' or
                                        self.type == 'in_refund'):
            if self.journal_id.purchase_type in (
                    'normal', 'informal',
                    'minor') or self.journal_id.ncf_control:
                ncf = self.origin_out
                if not ncf_validation.is_valid(ncf) and ncf[-10:-8] != '04':
                    msg = self._invoice_warnings('ncf_invalid')
                    raise UserError(_(msg % ncf))

    @api.model
    def create(self, vals):
        fiscal_type = vals.get('sale_fiscal_type')
        if fiscal_type and fiscal_type == "fiscal":
            partner_id = self.env["res.partner"].browse(vals['partner_id'])

            if partner_id and partner_id.vat:
                if len(partner_id.vat) not in [
                    9, 11
                ] or not rnc_validation.check_dgii(str(partner_id.vat)):
                    msg = self._invoice_warnings('rnc_invalid')
                    raise ValidationError(_(msg))

        return super(AccountInvoice, self).create(vals)

    @api.multi
    def action_invoice_open(self):
        for inv in self:
            if inv.ncf_id:
                ncf_valid = inv._ncf_sequence_valid()
                if not ncf_valid:
                    raise ValidationError("Esta secuencia de comprobante ha sido "
                                      "consumida en su totalidad, necesita "
                                      "pedir mas comprobantes a la DGII!")

            if inv.amount_untaxed == 0:
                msg = self._invoice_warnings('amount_cero')
                raise UserError(_(msg))

            if inv.type == "out_invoice" and inv.journal_id.ncf_control:
                partner_fiscal_type = inv.partner_id.sale_fiscal_type
                if not partner_fiscal_type:
                    msg = self._invoice_warnings('partner_fiscal')
                    raise ValidationError(_(msg % (inv.partner_id.id,
                                                   inv.partner_id.name)))
                if partner_fiscal_type == 'special':
                    empty_fields = self._get_special_empty_fields()
                    if empty_fields:
                        msg = self._invoice_warnings('ncf_special')
                        raise Warning(msg)

                if inv.sale_fiscal_type in (
                        "fiscal", "gov", "special") and not inv.partner_id.vat:
                    msg = self._invoice_warnings('partner_rnc')
                    raise UserError(_(msg % (inv.partner_id.id,
                                             inv.partner_id.name)))

                if (inv.amount_untaxed_signed >= 250000 and
                            inv.sale_fiscal_type not in ['export', 'unico'] and
                        not inv.partner_id.vat):
                    msg = self._invoice_warnings('amount_250k')
                    raise UserError(_(msg))
                inv._set_ncf_structure(inv.sale_fiscal_type)
            elif inv.type in ("in_invoice", "in_refund"):
                inv._set_ncf_structure(inv.journal_id.purchase_type)
                if inv.ncf_number and inv.journal_id.purchase_type in (
                        'normal', 'informal', 'minor', 'exterior'):
                    if not inv.partner_id.vat:
                        msg = self._invoice_warnings('not_rnc')
                        raise ValidationError(_(msg))

                    if (inv.journal_id.purchase_type == 'exterior' and
                                inv.partner_id.country_id.code == 'DO'):
                        msg = self._invoice_warnings('exterior')
                        raise ValidationError(_(msg))
            elif (inv.type == 'out_refund' and inv.journal_id.ncf_control and
                          inv.amount_untaxed_signed >= 250000 and
                      not inv.partner_id.vat):
                msg = self._invoice_warnings('nc_250k')
                raise ValidationError(_(msg))
        return super(AccountInvoice, self).action_invoice_open()

    @api.multi
    def action_move_create(self):
        res = super(AccountInvoice, self).action_move_create()
        for inv in self:
            if not inv.ncf_number and inv.ncf_id:
                sequence = inv.ncf_id.sequence_id
                if inv.type in {'out_refund', 'in_refund'}:
                    ncf_refund = inv.ncf_id.get_ncf_structure_for_refund(
                        inv.type)
                    sequence = ncf_refund.sequence_id
                if sequence:
                    sequence = sequence.with_context(
                        ir_sequence_date=inv.date or inv.date_invoice,
                    )
                    number = sequence.next_by_id()
                else:  # pragma: no cover
                    # Other localizations or not configured journals
                    number = inv.move_id.name
                inv.write({
                    'ncf_number': number,
                    'reference': number,
                    'move_name': number,
                })
                inv.move_id.write({'name': number})
        return res

    @api.model
    def _prepare_refund(
            self,
            invoice,
            date_invoice=None,
            date=None,
            description=None,
            journal_id=None):
        res = super(AccountInvoice, self)._prepare_refund(
            invoice,
            date_invoice=date_invoice,
            date=date,
            description=description,
            journal_id=journal_id,
        )

        if self.type == "out_invoice" and self.journal_id.ncf_control:
            res.update({"ncf_number": False, "origin_out": self.ncf_number})

        if self._context.get("credit_note_supplier_ncf", False):
            res.update({
                "ncf_number": self._context["credit_note_supplier_ncf"],
                "origin_out": self.ncf_number
            })
        return res

    @api.multi
    @api.constrains('state', 'tax_line_ids')
    def validate_special_exempt(self):
        """ Validates an invoice with Regímenes Especiales sale_fiscal_type
            does not contain nor ITBIS or ISC.

            See DGII Norma 05-19, Art 3 for further information.
        """
        for inv in self:
            if inv.type == 'out_invoice' and inv.state in (
                    'open', 'cancel') and inv.sale_fiscal_type == 'special':

                # If any invoice tax in ITBIS or ISC
                if any([
                        tax for tax in inv.tax_line_ids.mapped('tax_id')
                        .filtered(lambda tax: tax.tax_group_id.name in (
                            'ITBIS', 'ISC') and tax.amount != 0)
                ]):
                    msg = self._invoice_warnings('norma_05_19')
                    raise UserError(_(msg))

    def validate_fiscal_purchase(self):
        if self.ncf_number:
            NCF = self.ncf_number
            if self.journal_id.purchase_type == 'normal':
                if NCF[-10:-8] == '02':
                    self.ncf_number = False
                    msg = self._invoice_warnings('journal_normal')
                    raise ValidationError(_(msg % NCF))
                elif not ncf_validation.is_valid(NCF):
                    msg = self._invoice_warnings('ncf_invalid')
                    raise UserError(_(msg % NCF))
                elif not self.partner_id.vat:
                    msg = self._invoice_warnings('supplier_rnc')
                    raise ValidationError(_(msg % self.partner_id.name))
                elif (self.journal_id.ncf_remote_validation and
                      not ncf_validation.check_dgii(self.partner_id.vat, NCF)):
                    msg = self._invoice_warnings('supplier_ncf_invalid')
                    raise ValidationError(_(msg) % (NCF, self.partner_id.name))

                domain = [
                    ('partner_id', '=', self.partner_id.id),
                    ('ncf_number', '=', NCF),
                    ('state', 'in', ('draft', 'open', 'paid', 'cancel')),
                    ('type', 'in', ('in_invoice', 'in_refund')),
                ]
                if self.id:
                    domain.append(('id', '!=', self.id))
                    domain.append(('company_id', '=', self.company_id.id))
                    ncf_in_invoice = self.search_count(domain)
                else:
                    domain.append(('company_id', '=', self.company_id.id))
                    ncf_in_invoice = self.search_count(domain)

                if ncf_in_invoice:
                    msg = self._invoice_warnings('ncf_duplicate')
                    raise ValidationError(_(msg % NCF))

    def special_check(self):
        if self.sale_fiscal_type == "special":
            self.fiscal_position_id = self.ncf_id.fiscal_position_id.id
        else:
            self.fiscal_position_id = False

    @api.multi
    @api.constrains('state', 'invoice_line_ids', 'partner_id')
    def validate_products_export_ncf(self):
        """ Validates that an invoices with a partner from country != DO
            and products type != service must have Exportaciones NCF.

            See DGII Norma 05-19, Art 10 for further information.
        """
        for inv in self:
            if (inv.type == 'out_invoice' and
                    inv.state in ('open', 'cancel') and
                    inv.partner_id.country_id and
                    inv.partner_id.country_id.code != 'DO' and
                    inv.journal_id.ncf_control):
                if any([
                        p for p in inv.invoice_line_ids.mapped('product_id')
                        if p.type != 'service'
                ]):
                    if inv.sale_fiscal_type != 'export':
                        msg = self._invoice_warnings('foreign_sale')
                        raise UserError(_(msg))
                else:
                    if inv.sale_fiscal_type != 'final':
                        msg = self._invoice_warnings('foreign_sale_service')
                        raise UserError(_(msg))

    @api.constrains('state', 'tax_line_ids')
    def validate_informal_withholding(self):
        """ Validates an invoice with Comprobante de Compras has 100% ITBIS
            withholding.

            See DGII Norma 05-19, Art 7 for further information.
        """
        for inv in self:
            if (inv.type == 'in_invoice' and inv.state == 'open' and
                    inv.journal_id.purchase_type == 'informal'):
                # If the sum of all taxes of category ITBIS is not 0
                if sum([
                        tax.amount for tax in inv.tax_line_ids.mapped('tax_id')
                        .filtered(lambda t: t.tax_group_id.name == 'ITBIS')
                ]):
                    msg = self._invoice_warnings('informal_withholding')
                    raise UserError(_(msg))

    @api.multi
    def _set_ncf_structure(self, fiscal_type):
        """
        Search and establish the NCF structure corresponding to the
        type of voucher.
        """
        if not fiscal_type:
            raise ValidationError(_("Por favor defina el tipo de comprobante "
                                    "para este cliente"))
        elif fiscal_type == 'others':
            return self.update({'ncf_id': False})

        domain = [
            ('company_id', '=', self.company_id.id),
            ('journal_id', 'in', self.journal_id.ids)
        ]

        if self.type in ['out_invoice', 'out_refund']:
            domain.append(('type', '=', 'sale'))
            domain.append(('sale_type', '=', fiscal_type))
        else:
            domain.append(('type', '=', 'purchase'))
            domain.append(('purchase_type', '=', fiscal_type))

        ncf_id = self.env['ncf.manager'].search(domain, limit=1)
        if ncf_id:
            if fiscal_type != 'normal' and not ncf_id.change_log_ids:
                msg = self._invoice_warnings('ncf_structure')
                raise ValidationError(_(msg % fiscal_type.upper()))
            # active_authorization = ncf_id.change_log_ids.filtered(
            #     lambda l: l.state == 'active')
            # if not active_authorization:
            #     raise ValidationError(_("La autorizaci "))
            return self.update({'ncf_id': ncf_id.id})
        else:
            msg = self._invoice_warnings('ncf_structure')
            raise ValidationError(_(msg % fiscal_type.upper()))

    @api.model
    def _check_carnet_expiration_date(self):
        warning = {}
        if self.partner_id.parent_id and self.partner_id.parent_id.is_company:
            partner_id = self.partner_id.parent_id
        else:
            partner_id = self.partner_id

        if partner_id.expiration_date <= fields.Date.context_today(self):
            msg = self._invoice_warnings('special_info')
            warning.update({
                'title': _("Advertencia para Vencimiento Carnet Regímenes "
                           "Especiales"),
                'message': _(msg)
            })
        return warning

    @api.model
    def _get_special_empty_fields(self):
        special_info = self.partner_id.read([
            'cnzf_code', 'card_number', 'expiration_date', 'resolution_number'])
        empty_fields = [k for item in special_info
                        for k, v in item.items() if not v]

        return empty_fields

    @api.model
    def _invoice_warnings(self, reference):
        """Manage all warning needed."""
        warning_dict = {
            'ncf_expiration': 'El comprobante: %s, no cuenta con autorizacion',
            'minor': 'No existe un Diario de Gastos Menores, debe crear uno.',
            'ncf_invalid': "NCF mal digitado\n\n"
                           "El comprobante *%s* no tiene la estructura correcta"
                           " valide si lo ha digitado correctamente",
            'rnc_invalid': "El RNC del cliente NO pasó la validación en DGII"
                           "\n\nNo es posible crear una factura con Crédito "
                           "Fiscal si el RNC del cliente es inválido. Verifique"
                           " el RNC del cliente a fin de corregirlo y vuelva a "
                           "guardar la factura",
            'amount_cero': "No se puede validar una factura cuyo monto total "
                           "sea igual a 0.",
            'partner_fiscal': "El cliente [%s]%s no tiene este Tipo de comprobante, "
                              "y es requerido para este tipo de factura.",
            'ncf_special': "Las informaciones de Regimen Especial no estan "
                            "completas, deberá llenarlas para poder continuar",
            'partner_rnc': "El cliente [%s]%s no tiene RNC/Céd, y es requerido"
                           " para este tipo de factura.",
            'amount_250k': "Si el monto es mayor a RD$250,000 el cliente debe "
                           "tener un RNC o Céd para emitir la factura",
            'not_rnc': '¡Para este tipo de Compra el Proveedor debe de tener un'
                       ' RNC/Cédula/NIT establecido!',
            'exterior': '¡Para Remesas al Exterior el Proveedor debe tener país'
                        ' diferente a República Dominicana!',
            'nc_250k': 'Para poder emitir una NC mayor a RD$250,000 se requiere'
                       ' que el cliente tenga RNC o Cédula.',
            'norma_05_19': "No puede validar una factura para Regímen Especial "
                           "con ITBIS/ISC.\n\n Consulte Norma General 05-19, "
                           "Art. 3 de la DGII",
            'journal_normal': "NCF *{}* NO corresponde con el tipo de documento"
                              "\n\n No puede registrar Comprobantes Consumidor"
                              " Final (02)",
            'supplier_rnc': "Proveedor sin RNC/Céd\n\n El proveedor *%s* no "
                            "tiene RNC o Cédula y es requerido para registrar"
                            " compras Fiscales",
            'supplier_ncf_invalid': "NCF NO pasó validación en DGII\n\n "
                                    "¡El número de comprobante *%s* del "
                                    "proveedor *%s* no pasó la validación en "
                                    "DGII! Verifique que el NCF y el RNC del "
                                    "proveedor estén correctamente digitados, "
                                    "o si los números de ese NCF se le agotaron"
                                    " al proveedor",
            'ncf_duplicate': "NCF Duplicado en otra Factura\n\nEl comprobante"
                             " *%s* ya se encuentra registrado con este mismo"
                             " proveedor en una factura en borrador o "
                             "cancelada",
            'foreign_sale': "La venta de bienes a clientes extranjeros deben "
                       "realizarse con comprobante tipo Exportaciones",
            'foreign_sale_service': "La venta de servicios a clientes "
                                    "extranjeros deben realizarse con "
                                    "comprobante tipo Consumo",
            'informal_withholding': "Debe retener el 100% del ITBIS",
            'ncf_structure': "El tipo de comprobante de este cliente es: %s, y "
                             "usted no cuenta con una estructura para este tipo"
                             " de comprobante o con una autorización de DGII "
                             "para el mismo. Por favor, contacte a su Asesor "
                             "Contable",
            'special_info': "El carnet de este cliente se encuentra vencido!",
        }

        return warning_dict[reference]

    @api.multi
    def _ncf_sequence_valid(self):
        if not self.ncf_id:
            return {}

        number_next = self.ncf_id.sequence_id.number_next_actual
        active_auths = self.ncf_id.change_log_ids.search([
            ('state', '=', 'active'),
            ('ncf_id', '=', self.ncf_id.id),
        ])
        valid = True
        for auth in active_auths:
            if number_next > auth.to_number:
                auth.write({'state': 'expired'})
                valid = False
            if number_next <= auth.to_number:
                valid = True
        return valid
