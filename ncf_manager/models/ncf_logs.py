from odoo import api, fields, models, _
from odoo.exceptions import ValidationError


class NcfChangeLog(models.Model):
    _name = 'ncf.change.log'
    _description = "Maintains a record of the updates of the fiscal sequences."

    name = fields.Char(
        string='Nombre',
        readonly=True,
        store=True,
        copy=False,
        default=lambda x: _('New'),
    )
    reason = fields.Char(string='Razón', required=True, readonly=True, )
    ncf_id = fields.Many2one(
        comodel_name='ncf.manager',
        readonly=True,
        string="Comprobante",
    )
    request_no = fields.Char(
        string='No. Solicitud',
        readonly=True,
        copy=False,
    )
    request_date = fields.Date(
        string='Fecha de solicitud',
        readonly=True,
        copy=False,
    )
    auth_number = fields.Char(
        string='No. Autorización',
        readonly=True,
        copy=False,
    )
    from_number = fields.Integer(
        string='Número Desde',
        readonly=True,
        copy=False,
    )
    to_number = fields.Integer(
        string='Número Hasta',
        readonly=True,
        copy=False,
    )
    due_date = fields.Date(
        string='Fecha Vencimiento',
        readonly=True,
        copy=False,
    )
    update_by = fields.Many2one(
        comodel_name='res.users',
        readonly=True,
        copy=False,
        string="Usuario",
    )
    company_id = fields.Many2one(
        comodel_name='res.company',
        string='Company',
        change_default=True,
        required=True,
        readonly=True,
        copy=False,
        default=lambda self: self.env['res.company']._company_default_get(
            'ncf.change.log'),
    )
    ncf_seq_percent = fields.Float(related='company_id.ncf_seq_percent')
    state = fields.Selection(
        string="State",
        selection=[
            ("active", "Active"),
            ("waiting", "Wating"),
            ("expired", "Expired"),
        ],
        default="active",
        copy=False,
        readonly=True,
    )
    next_authorization = fields.Boolean(string='Proxima autorización', )

    @api.multi
    @api.constrains('from_number', 'to_number')
    def check_numbers(self):
        last_authorization = self.search([
            ('ncf_id', '=', self.ncf_id.id),
            ('state', '=', 'active'),
        ], order="id desc", limit=1)
        if last_authorization:
            for record in self:
                if last_authorization.to_number < record.from_number:
                    raise ValidationError(_("Número Desde no puede ser menor "
                                            "a Número Hasta de la autorización"
                                            " vigente con el número: %s" % 
                                            last_authorization.to_number))
                if record.to_number < record.from_number:
                    raise ValidationError(_("Número Hasta no puede ser menor a "
                                            "Número Desde"))

    @api.model
    def create(self, values):
        if not values.get('name', False) or values['name'] == _('New'):
            values['name'] = self.env['ir.sequence'].next_by_code(
                'ncf.change.log') or _('New')
        res = super(NcfChangeLog, self).create(values)
        
        for rec in res: 
            alert_number = rec.set_alert_number()
            if alert_number:
                rec.ncf_id.write({'ncf_alert_number': alert_number})

            rec.ncf_id.sequence_id.write({
                'number_next_actual': rec.from_number
            })
            rec.ncf_id._compute_sequence_next()

        return res
    
    def set_alert_number(self):
        alert_number = 0
        if self.to_number and self.ncf_seq_percent:
            alert_number = self.to_number * (self.ncf_seq_percent / 100)

        return alert_number