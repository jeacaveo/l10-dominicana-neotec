# © 2019 Yasmany Castillo <yasmany003@gmail.com>

# This file is part of NCF Manager.

# NCF Manager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# NCF Manager is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with NCF Manager.  If not, see <https://www.gnu.org/licenses/>.

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError


READONLY_STATES = {
    'draft': [('readonly', False)],
    'confirmed': [('readonly', True)],
    'cancel': [('readonly', True)],
}

NCF_TYPE = {
    'normal': ('01', 'Compras Fiscales'),
    'fiscal': ('01', 'Crédito Fiscal'),
    'final': ('02', 'Consumo'),
    'debit_note': ('03', 'Nota de Débito'),
    'credit_note': ('04', 'Nota de Crédito'),
    'informal': ('11', 'Comprobante de Compras'),
    'unico': ('12', 'Registro Único de Ingresos'),
    'minor': ('13', 'Comprobante para Gastos Menores'),
    'special': ('14', 'Comprobante para Regímenes Especiales'),
    'gov': ('15', 'Comprobantes Gubernamentales'),
    'export': ('16', 'Comprobantes para Exportaciones'),
    'exterior': ('17', 'Comprobantes de Pagos al Exterior'),
    # 'import': ('E', 'Comprobante Fiscal Electrónico'),
    'others': ('SCF', 'Sin Comprobante Fiscal'),
}


class NcfManager(models.Model):
    """
    This model contains the necessary information to be able to handle the
    receipts of the DGII in a simple way.
    """
    _name = 'ncf.manager'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = "NCF Structure"
    _order = "number_next desc, id desc"

    @api.multi
    @api.depends('state', 'sequence_id')
    def _compute_sequence_next(self):
        """Set the number_next on the sequence related to this NCF."""
        for ncf in self:
            if ncf.state not in ['draft', 'cancel'] and ncf.sequence_id:
                seq = ncf.sequence_id._get_current_sequence()
                number_next = seq.number_next_actual
                sequence = '%%0%sd' % seq.padding % number_next
                ncf.number_next = seq.prefix + sequence

    name = fields.Char(
        string='Name',
        required=True,
        readonly=True,
        store=True,
        copy=False,
        default=lambda self: _('New'),
    )
    number_next = fields.Char(
        string='Next Number',
        readonly=True,
        copy=False,
        compute="_compute_sequence_next",
    )
    type = fields.Selection(
        string='NCF for',
        selection=[
            ("sale", "Sale"),
            ("purchase", "Purchase"),
        ],
        default="",
        required=True,
        readonly=True,
        states=READONLY_STATES,
    )
    sale_type = fields.Selection(
        string='Sale type',
        selection=[
            ("final", "Consumo"),
            ("fiscal", u"Crédito Fiscal"),
            ("gov", "Gubernamentales"),
            ("special", u"Regímenes Especiales"),
            ("unico", u"Único Ingreso"),
            ("export", u"Exportaciones"),
            ("credit_note", "Credit Note"),
        ],
        default="",
        copy=False,
        readonly=True,
        states=READONLY_STATES,
    )
    purchase_type = fields.Selection(
        string="Purchase type",
        selection=[
            ("normal", "Compras Fiscales"),
            ("minor", "Gastos Menores"),
            ("informal", "Comprobante de Compras"),
            ("exterior", "Pagos al Exterior"),
            ("debit_note", "Debit Note"),
        ],
        default="",
        readonly=True,
        states=READONLY_STATES,
        copy=False,
    )
    journal_id = fields.Many2many(
        comodel_name='account.journal',
        relation="ncf_manager_account_journal_rel",
        column1="ncf_id",
        column2="journal_id",
        string='Journal',
        required=True,
        readonly=True,
        states=READONLY_STATES,
        ondelete='restrict',
    )
    sequence_id = fields.Many2one(
        comodel_name='ir.sequence',
        string='Sequence',
        readonly=True,
        required=False,
        ondelete='cascade',
    )
    fiscal_position_id = fields.Many2one(
        comodel_name='account.fiscal.position',
        string='Fiscal Position',
        readonly=True,
        states={'draft': [('readonly', False)]},
    )
    state = fields.Selection(
        string="State",
        selection=[
            ("draft", "Draft"),
            ("confirmed", "Confirmed"),
            ("cancel", "Cancel"),
        ],
        default="draft",
        copy=False,
    )
    company_id = fields.Many2one(
        comodel_name='res.company',
        string='Company',
        change_default=True,
        required=True,
        readonly=True,
        default=lambda self: self.env['res.company']._company_default_get(
            'ncf.manager'),
    )
    active = fields.Boolean(string='Active', default=True, )
    invoice_ids = fields.One2many(
        comodel_name='account.invoice',
        inverse_name='ncf_id',
        string='Invoices',
        required=False,
        readonly=True,
    )
    ncf_alert_number = fields.Float(
        string='Alert number',
        help="When sequence equal to this number an alert will be send", )
    change_log_ids = fields.One2many(
        comodel_name="ncf.change.log",
        inverse_name="ncf_id",
        readonly=True,
        string="Historico de secuencias",
    )

    _sql_constraints = [
        ('sale_uniq', 'unique(sale_type, company_id)',
         'Sale type must be unique per company!'),
        ('purchase_uniq', 'unique(purchase_type, company_id)',
         'Purchase type must be unique per company!'),
    ]

    @api.onchange('journal_id', 'type')
    def _onchange_journal_id(self):
        Journal = self.env['account.journal']
        journal_domain = [
            ('active', '=', True),
            ('company_id', '=', self.env.user.company_id.id),
        ]

        if self.type:
            if self.type == 'sale':
                journal_domain.append(('type', '=', 'sale'))
                journals = Journal.search(journal_domain)
            else:
                journal_domain.append(('type', '=', 'purchase'))
                journals = Journal.search(journal_domain)

            return {
                'domain': {
                    'journal_id': [('id', 'in', journals.ids)]
                }
            }

    @api.multi
    @api.depends('type', 'sale_type', 'purchase_type', 'credit_note_control')
    def name_get(self):
        """Set how the name should be displayed."""
        result = []
        for record in self:
            ncf_type = record.sale_type or record.purchase_type
            name = NCF_TYPE.get(ncf_type)[1]
            result.append((record.id, name))
        return result

    @api.model
    def create(self, values):
        """Override create method to to set name according to NCF type"""
        ncf_type = values.get('sale_type') or values.get('purchase_type')
        values['name'] = NCF_TYPE.get(ncf_type)[1]
        return super(NcfManager, self).create(values)

    @api.multi
    def unlink(self):
        """Allow to remove ncf."""
        for record in self:
            ncf_in_use = self.env['account.invoice'].search([
                ('ncf_id', '=', record.id)])
            if ncf_in_use:
                raise UserError(
                    _("You can only delete a record that has not been used in "
                      "invoices.."))

            if record.state != 'draft':
                raise UserError(
                    _("You only can delete a record in draft state."))
        return super(NcfManager, self).unlink()

    @api.multi
    def action_cancel(self):
        """Cancel record"""
        return self.write({'state': 'cancel'})

    @api.multi
    def action_set_to_draft(self):
        """Change record to draft to allow modify record"""
        return self.write({'state': 'draft'})

    @api.multi
    def create_sequence(self):
        """Create a new sequence if not exist for this ncf."""
        sequence_obj = self.env['ir.sequence']
        sequence = sequence_obj.search(
            [
                ('company_id', '=', self.company_id.id),
                ('ncf_id', '=', self.id)
            ],
        )
        if sequence:
            return self.write({
                'sequence_id': sequence.id,
                'state': 'confirmed',
            })

        sequence_values = {
            'implementation': 'no_gap',
            'padding': 8,
            'number_increment': 1,
            'use_date_range': False,
            'company_id': self.company_id.id,
            'ncf_id': self.id,
        }

        ncf_type = self.sale_type or self.purchase_type
        sequence_values['name'] = NCF_TYPE.get(ncf_type)[1]
        sequence_values['prefix'] = "B%s" % NCF_TYPE.get(ncf_type)[0]

        sequence_id = sequence_obj.create(sequence_values)
        return self.write({
            'sequence_id': sequence_id.id,
        })

    @api.multi
    def configure(self):
        """Configure a sequence and journal for this ncf."""
        journal_values = {}
        if self.type == 'sale':
            self.create_sequence()
            if self.sale_type in ['final', 'fiscal']:
                journal_values['ncf_control'] = True
        else:
            if self.purchase_type == 'normal':
                journal_values['purchase_type'] = self.purchase_type
                journal_values['ncf_remote_validation'] = True
            else:
                self.create_sequence()
                journal_values['purchase_type'] = self.purchase_type
        for journal in self.journal_id:
            journal.write(journal_values)
        return self.write({'state': 'confirmed'})

    @api.multi
    def get_ncf_structure_for_refund(self, invoice_type):
        """Return NCF for refund"""
        if invoice_type not in ['out_refund', 'in_refund']:
            raise ValidationError(_("This invoice type must be a refund type."))

        if invoice_type == 'out_refund':
            ncf_structure = self.search([
                ('type', '=', 'sale'),
                ('sale_type', '=', 'credit_note'),
            ])
        else:
            ncf_structure = self.search([
                ('type', '=', 'purchase'),
                ('purchase_type', '=', 'debit_note'),
            ])

        return ncf_structure

    @api.model
    def _ncf_cron_sequence_alert(self):
        """Search all ncf structure and send alert if necessary."""
        records = self.search([
            ('state', '=', 'confirmed'),
            ('type', '=', 'sale')
        ])

        for ncf in records:
            seq_id = ncf.sequence_id
            alert_number = ncf.ncf_alert_number
            if seq_id and alert_number:
                if seq_id.number_next_actual >= alert_number:
                    msg = "Alerta secuencias agotadas de: "
                    model_id = self.env.ref('ncf_manager.model_ncf_manager').id
                    activity_type = self.env['mail.activity.type'].search([
                        ('name', '=', 'Email'),
                    ])
                    ncf_group = 'ncf_manager.group_ncf_manager'
                    groups = self.env['res.groups'].concat(*(
                        self.env.ref(it) for it in ncf_group.split(',')))

                    for user in groups.users:
                        ncf.activity_ids.create({
                            'res_model_id': model_id,
                            'res_id': ncf.id,
                            'user_id': user.id,
                            'activity_type_id': activity_type.id,
                            'summary': msg + ncf.name,
                            'date_deadline': fields.Date.today(),
                        })

        return True
